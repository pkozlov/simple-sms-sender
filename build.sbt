name := "simple-sms-sender"

version := "1.0"

scalaVersion := "2.10.2"

libraryDependencies += "net.databinder.dispatch" %% "dispatch-core" % "0.11.0"

libraryDependencies += "net.sourceforge.htmlcleaner" % "htmlcleaner" % "2.6"
