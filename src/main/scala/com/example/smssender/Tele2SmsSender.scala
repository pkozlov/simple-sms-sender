package com.example.smssender

import org.htmlcleaner._
import java.net.URL
import scala.collection.JavaConversions._
import java.io.{BufferedReader, File}
import sys.process._
import dispatch._
import Defaults._
import scala.io._

class Tele2SmsSender extends SmsSender {

  private val cleaner = new HtmlCleaner
  private val reader: BufferedReader = Source.fromInputStream(stdin).bufferedReader()

  private val form = {
    val node = Http(url("http://www.ru.tele2.ru/send_sms.html")).either() match {
      case Right(content) => cleaner.clean(content.getResponseBodyAsStream)
      case Left(StatusCode(code)) => throw new IllegalStateException("error " + code)
    }
    // TODO replace with XPATH expression
    node.findElementByAttValue("name", "sendsms", true, true)
  }

  private val phoneCodes = (for {
  // TODO replace with XPATH expression
    child <- form.findElementByAttValue("name", "phone_code", true, true).getElementListByName("option", false)
    code = child match {
      case node: TagNode => node.getText.toString
      case _ => ""
    }
    if (!code.isEmpty)
  } yield code).toSet

  // TODO replace with XPATH expression
  private val publicKey = form.findElementByAttValue("name", "public_key", true, true).getAttributeByName("value")

  new URL(s"http://new.tools.tele2.ru/get_rni.php?public_key=$publicKey") #> new File("captcha.png") !!

  def send(phoneCode: String, number: String, text: String) {
    assert(phoneCodes.contains(phoneCode), s"phone code must be one of $phoneCodes")
    assert(number.length == 7, "number length must have 7 digits")

    // TODO replace with something better than reading from console
    print("Type private key (from captcha.png): ")
    val privateKey = reader.readLine()
    val headers = Map("Content-Type" -> "application/x-www-form-urlencoded")
    val params = s"do=send&smstext=$text&phone_code=$phoneCode&number=$number&private_key=$privateKey&public_key=$publicKey"

    Http(url("http://www.ru.tele2.ru/send_sms.html") << params <:< headers).either() match {
      case Right(content) => {
        val node = cleaner.clean(content.getResponseBodyAsStream)
        // TODO replace with XPATH expression
        node.findElementByAttValue("class", "text-content", true, true).findElementByName("p", false) match {
          case p: TagNode => println(p.findElementByName("span", true).getText)
          case _ => println("no p")
        }
      }
      case Left(StatusCode(code)) => println(s"error code: $code")
    }
    Http.shutdown()
  }

}
