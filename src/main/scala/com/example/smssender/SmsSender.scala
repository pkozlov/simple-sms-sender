package com.example.smssender

trait SmsSender {

  def send(phoneCode: String, number: String, text: String)

}
